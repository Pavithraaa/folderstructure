import React, { useState } from "react";

const TreeNode = ({ node, onNodeClick, onCreateNode, onDeleteNode, onEditNode }) => {
  const [isEditing, setIsEditing] = useState(false);
  const [editedName, setEditedName] = useState(node.name);

  const handleToggle = () => {
    if (node.type === "folder") {
      onNodeClick(node);
    }
  };

  const handleEditClick = () => {
    setIsEditing(true);
  };

  const handleSaveEdit = () => {
    setIsEditing(false);
    onEditNode(editedName);
  };

  const handleCancelEdit = () => {
    setIsEditing(false);
    setEditedName(node.name);
  };

  return (
    <div className={`tree-node ${node.type}`}>
      <div className="node-content">
        <span onClick={handleToggle} className="toggle-arrow">
          {node.type === "folder" && (node.expanded ? "▼" : "►")}
        </span>
        {isEditing ? (
          <input
            type="text"
            value={editedName}
            onChange={(e) => setEditedName(e.target.value)}
          />
        ) : (
          <span>{node.name}</span>
        )}
        <div className="node-actions">
          <button onClick={handleEditClick}>Edit</button>
          <button onClick={onCreateNode}>Create</button>
          <button onClick={onDeleteNode}>Delete</button>
        </div>
      </div>
      {node.expanded && (
        <div className="child-nodes">
          {node.children.map((childNode, index) => (
            <TreeNode
              key={index}
              node={childNode}
              onNodeClick={onNodeClick}
              onCreateNode={() => onCreateNode(childNode)}
              onDeleteNode={() => onDeleteNode(index)}
              onEditNode={onEditNode}
            />
          ))}
        </div>
      )}
    </div>
  );
};

export default TreeNode;
