import React, { useState } from "react";
import TreeNode from "./TreeNode";

const FolderStructure = ({ data }) => {
  const [treeData, setTreeData] = useState(data);

  const handleNodeClick = (node) => {
    // Toggle the node's expanded state
    node.expanded = !node.expanded;
    setTreeData([...treeData]);
  };

  const handleCreateNode = (parentNode) => {
    const newNode = {
      name: "New Folder",
      type: "folder",
      expanded: true,
      children: [],
    };

    parentNode.children.push(newNode);
    setTreeData([...treeData]);
  };

  const handleDeleteNode = (parentNode, index) => {
    parentNode.children.splice(index, 1);
    setTreeData([...treeData]);
  };

  const handleEditNode = (node, newName) => {
    node.name = newName;
    setTreeData([...treeData]);
  };

  return (
    <div className="folder-structure">
      {treeData.map((node, index) => (
        <TreeNode
          key={index}
          node={node}
          onNodeClick={handleNodeClick}
          onCreateNode={() => handleCreateNode(node)}
          onDeleteNode={(index) => handleDeleteNode(node, index)}
          onEditNode={(newName) => handleEditNode(node, newName)}
        />
      ))}
    </div>
  );
};

export default FolderStructure;
