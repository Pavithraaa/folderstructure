import React from "react";
import FolderStructure from "./FolderStructure";

const jsonData = [
  {
    name: "Folder 1",
    type: "folder",
    expanded: true,
    children: [
      { name: "File 1", type: "file" },
      {
        name: "Subfolder 1",
        type: "folder",
        expanded: true,
        children: [{ name: "File 2", type: "file" }],
      },
    ],
  },
];

function App() {
  return (
    <div className="App">
      <FolderStructure data={jsonData} />
    </div>
  );
}

export default App;
